package com.rsreu.EPDApplication.repositories;

import com.rsreu.EPDApplication.entities.UserRole;
import com.rsreu.EPDApplication.entities.UserRoleKey;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRoleRepo extends CrudRepository<UserRole, UserRoleKey> {


}
