package com.rsreu.EPDApplication.repositories;

import com.rsreu.EPDApplication.entities.UserFile;
import com.rsreu.EPDApplication.entities.UserFileKey;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserFileRepo extends CrudRepository<UserFile, UserFileKey> {

}
