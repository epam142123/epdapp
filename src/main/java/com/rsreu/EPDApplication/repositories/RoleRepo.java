package com.rsreu.EPDApplication.repositories;

import com.rsreu.EPDApplication.entities.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepo extends CrudRepository<Role, Long> {
}
