package com.rsreu.EPDApplication.controllers;

import com.rsreu.EPDApplication.EpdApplication;
import com.rsreu.EPDApplication.entities.FileXML;
import com.rsreu.EPDApplication.entities.User;
import com.rsreu.EPDApplication.entities.UserFileKey;
import com.rsreu.EPDApplication.repositories.KeyPairRepo;
import com.rsreu.EPDApplication.repositories.UserFileRepo;
import com.rsreu.EPDApplication.services.FileXMLService;
import com.rsreu.EPDApplication.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

@Controller
@RequestMapping("/file")
public class FileController {

    @Autowired
    private FileXMLService service;
    @Autowired
    private KeyPairRepo keyRepo;
    @Autowired
    private UserService userService;
    @Autowired
    private UserFileRepo userFileRepo;

    @GetMapping("/{id}")
    public ResponseEntity getFile(@PathVariable Long id, Authentication authentication) {
        try {
            User user = userService.findByLogin(authentication.getName());
            FileXML file = service.findById(id);
            if (userFileRepo.existsById(new UserFileKey(user.getId(), file.getId()))) {
                String result = new String(file.getFile().getBytes(), StandardCharsets.UTF_8);
                return ResponseEntity.ok().body(result);
            } else {
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/init")
    public ResponseEntity init() {
        try {
//            RSAKeyPairGenerator generator = new RSAKeyPairGenerator();
//            generator.init(new RSAKeyGenerationParameters(BigInteger.valueOf(17), SecureRandom.getInstance("SHA1PRNG"),35, 62));
//            AsymmetricCipherKeyPair keyPair = generator.generateKeyPair();
//            keyRepo.save(MyKeyPair.builder().mKey(keyPair.toString().getBytes()).build());
            for (long i = 1; i <= 100; i++) {
                String path = "epd/ON_TRNACLGROT_" + i + ".xml";
                FileXML file = new FileXML();
                file.setId(i);
                InputStreamReader reader = new InputStreamReader(Objects.requireNonNull(EpdApplication.class.getClassLoader()
                        .getResourceAsStream(path)), "windows-1251");
                StringBuilder builder = new StringBuilder();
                while (reader.ready()) {
                    builder.append((char) reader.read());
                }
                file.setFile(builder.toString());
                service.save(file);
            }
            return ResponseEntity.ok().body("ok");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PostMapping("/{id}")
    public ResponseEntity uploadFile(@PathVariable Long id, @RequestBody String text) {
        try {
            FileXML xml = FileXML.builder().id(id).file(text).build();
            service.save(xml);
            return ResponseEntity.ok().body("File uploaded");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

//    @GetMapping("/crt/{id}")
//    public ResponseEntity getCertificate(@PathVariable Long id) {
//        try {
//            PEMParser parser = new PEMParser(new StringReader(new String(keyRepo.findById(1L).orElseThrow().getMKey())));
//            PEMKeyPair keyPair = (PEMKeyPair) parser.readObject();
//            byte[] encoder = keyPair.getPrivateKeyInfo().getEncoded();
//            KeyFactory kf = KeyFactory.getInstance("RSA");
//            X509V3CertificateGenerator generator = new X509V3CertificateGenerator();
//            generator.setSerialNumber(BigInteger.valueOf(id));
//            generator.setSubjectDN(new X509Name("CN=CA"));
//            generator.setIssuerDN(new X509Name("CN=CA"));
//            generator.setNotAfter(Date.from(LocalDateTime.now().plusYears(100L).atZone(ZoneId.systemDefault()).toInstant()));
//            generator.setNotBefore(new Date());
//            generator.setSignatureAlgorithm("SHA1WITHRSA");
//            generator.setPublicKey(kf.generatePublic(new PKCS8EncodedKeySpec(encoder)));
//            X509Certificate cert = generator.generate(kf.generatePrivate(new PKCS8EncodedKeySpec(encoder)));
//            return ResponseEntity.ok(cert.toString());
//        } catch (Exception e) {
//            return ResponseEntity.badRequest().body(e.toString());
//        }
//    }
}