package com.rsreu.EPDApplication.dto;

import com.rsreu.EPDApplication.entities.User;
import com.rsreu.EPDApplication.entities.UserRole;
import lombok.*;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UserDTO {

    private Long id;
    private String name;
    private String login;
    private List<String> roles;

    public static UserDTO toDTO(User user) {
        return UserDTO.builder()
                .id(user.getId())
                .name(user.getName())
                .login(user.getUsername())
                .roles(user.getRoles().stream().map(x -> x.getRole().getName()).collect(Collectors.toList()))
                .build();
    }
}
