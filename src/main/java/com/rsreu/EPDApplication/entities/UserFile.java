package com.rsreu.EPDApplication.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "user_file")
@NoArgsConstructor
@AllArgsConstructor
public class UserFile {
    @EmbeddedId
    @Getter @Setter
    private UserFileKey key;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user")
    @MapsId("user")
    @Getter @Setter
    private User user;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "file")
    @MapsId("file")
    @Getter @Setter
    private FileXML file;
}
