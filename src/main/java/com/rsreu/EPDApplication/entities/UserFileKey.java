package com.rsreu.EPDApplication.entities;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class UserFileKey implements Serializable {

    @Column(name = "user")
    private Long user;
    @Column(name = "role")
    private Long role;

}
