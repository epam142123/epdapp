package com.rsreu.EPDApplication.entities;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "files")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FileXML {

    @Id @Column
    private Long id;
    @Column
    private String file;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "file")
    private Set<UserFile> users;
}
