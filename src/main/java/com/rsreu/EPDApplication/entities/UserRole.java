package com.rsreu.EPDApplication.entities;


import liquibase.repackaged.org.apache.commons.lang3.builder.ToStringExclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "user_role")
@NoArgsConstructor
@AllArgsConstructor
public class UserRole {

    @EmbeddedId
    @Getter @Setter
    private UserRoleKey key;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user")
    @MapsId("user")
    @Getter @Setter
    private User user;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "role")
    @MapsId("role")
    @Getter @Setter
    private Role role;
}
