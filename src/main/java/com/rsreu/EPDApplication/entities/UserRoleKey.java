package com.rsreu.EPDApplication.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class UserRoleKey implements Serializable {

    @Column(name = "user")
    private User user;
    @Column(name = "role")
    private Role role;
}
