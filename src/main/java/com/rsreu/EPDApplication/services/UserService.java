package com.rsreu.EPDApplication.services;

import com.rsreu.EPDApplication.dto.UserDTO;
import com.rsreu.EPDApplication.entities.User;
import com.rsreu.EPDApplication.repositories.RoleRepo;
import com.rsreu.EPDApplication.repositories.UserRepo;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
@AllArgsConstructor
@NoArgsConstructor
public class UserService implements UserDetailsService {

    @Autowired
    UserRepo repo;
    @Autowired
    RoleRepo roleRepo;
    @Autowired
    PasswordEncoder encoder;

    public void save(User user) {
        if (repo.findByUsername(user.getUsername()) == null) {
            user.setPassword(encoder.encode(user.getPassword()));
            repo.save(user);
        } else {
            throw new IllegalArgumentException("Login is not available");
        }
    }

    public User findById(Long id) {
        return repo.findById(id).orElseThrow();
    }

    public User findByLogin(String login) {
        User user = repo.findByUsername(login);
        if (user != null) {
            return user;
        } else {
            throw new NoSuchElementException();
        }
    }

    public void delete(Long id) {
        repo.findById(id).ifPresent(value -> repo.delete(value));
    }

    public void update(UserDTO user, Long id) {
        User user1 = repo.findById(id).orElseThrow();
        user1.setName(user.getName());
        user1.setUsername(user.getLogin());
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = repo.findByUsername(username);
        if (user != null) {
            return user;
        }
        throw new UsernameNotFoundException("User not found");
    }
}
